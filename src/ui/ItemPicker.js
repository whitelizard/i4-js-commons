import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  items: PropTypes.objectOf(PropTypes.any),
  dataSourceConfig: PropTypes.objectOf(PropTypes.string),
  addChipCallback: PropTypes.func.isRequired,
  deleteChipCallback: PropTypes.func.isRequired,
  selectedItems: PropTypes.arrayOf(PropTypes.string),
  floatingLabelText: PropTypes.string,
  disabled: PropTypes.bool,
};

const defaultProps = {
  items: {},
  selectedItems: [],
  disabled: false,
  floatingLabelText: 'Select Items',
  dataSourceConfig: { text: 'name', value: 'id' },
};

const styles = theme => ({
  selectField: {
    minWidth: 180,
    maxWidth: 256,
    display: 'block',
    marginTop: theme.spacing.unit,
  },
  chip: { marginTop: theme.spacing.unit, marginRight: theme.spacing.unit },
  chips: {
    display: 'flex',
    [theme.breakpoints.down('md')]: {
      maxWidth: 512,
    },
    flexWrap: 'wrap',
    marginTop: theme.spacing.unit * 2,
  },
});

const hoc = withStyles(styles);

const ItemPickerComponent = ({
  dataSourceConfig: { text: name, value },
  items,
  selectedItems,
  addChipCallback,
  deleteChipCallback,
  floatingLabelText,
  disabled,
  classes,
}) => {
  const handleSelect = newItems => {
    if (newItems.length > selectedItems.length) {
      const newItem = newItems.find(i => !selectedItems.includes(i));
      if (newItem) addChipCallback(newItem);
    } else if (newItems.length < selectedItems.length) {
      const deletedItem = selectedItems.find(i => !newItems.includes(i));
      deleteChipCallback(deletedItem);
    }
  };
  return (
    <div>
      <FormControl disabled={disabled}>
        <InputLabel htmlFor="select">{floatingLabelText}</InputLabel>
        {selectedItems.length >= 1 && (
          <div className={classes.chips}>
            {selectedItems.map(id => (
              <Chip
                label={R.pathOr('No label', [id, name], items)}
                key={id}
                onDelete={disabled ? undefined : () => deleteChipCallback(id)}
                className={classes.chip}
              />
            ))}
          </div>
        )}
        <Select
          onChange={e => handleSelect(e.target.value)}
          value={selectedItems}
          multiple
          className={classes.selectField}
          input={<Input name="select" id="select" />}
        >
          {Object.values(items).map((item, ix) => {
            const { [value]: val = ix, [name]: label = 'No name' } = item;
            return (
              <MenuItem key={val} value={val}>
                {label}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
};

ItemPickerComponent.propTypes = propTypes;
ItemPickerComponent.defaultProps = defaultProps;

export const ItemPicker = hoc(ItemPickerComponent);
