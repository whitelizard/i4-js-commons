/**
 * Usage:
 * <SelectionBox
 *  label   // [Optional] Form control label.
 *  data    // An array of data to be enumerated.
 *  map     // [Optinal] Callback function used to gererate the option,
 *          // otherwise keys: label & value are used.
 * strings  // [Optinal] data values are unique strings and map function will be ignored.
 *  ...     // The other props belongs to a <Select> element.
 * />
 *
 * Remarks: The map function will be called once for each element in the data array. Expected output
 *  from the map function is an object with a value and label key:
 *  { value: "unique value", label: "What to show in the menu" }
 */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { FormControl, InputLabel, Select, MenuItem, Input } from '@material-ui/core';

const styles = () => ({
  // form: { display: 'inline-block' },
  select: { minWidth: 180 },
});

function SelectionBoxC({ classes, label, className, data, map, strings, onChange, ...rest }) {
  const [value, setValue] = useState([]);

  useEffect(() => {
    if (rest.value && rest.value !== value) setValue(rest.value);
  }, [rest.value]);

  const hChange = e => {
    setValue(e.target.value);
    if (onChange) onChange(e, e.target.value);
  };

  return (
    <FormControl className={className}>
      {label && <InputLabel htmlFor="selector">{label}</InputLabel>}
      <Select
        onChange={hChange}
        className={classes.select}
        input={<Input name="thing" id="selector" />}
        value={value}
        {...rest}
      >
        {data.map(entry => {
          let info = entry;
          if (strings) {
            info = { value: entry.toString(), label: entry.toString() };
          } else if (typeof map === 'function') {
            info = map(info);
          } else if (typeof map === 'object') {
            if (map.value) {
              info = {
                value: info[map.value],
                label: info[map.label || map.value],
              };
            } else {
              info = { value: entry.toString(), label: entry.toString() };
            }
          }
          return (
            <MenuItem key={info.value} value={info.value} disableRipple>
              {info.label}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}

SelectionBoxC.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  label: PropTypes.string,
  className: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.any).isRequired,
  map: PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.any)]),
  strings: PropTypes.bool,
  onChange: PropTypes.func,
};

SelectionBoxC.defaultProps = {
  label: undefined,
  onChange: undefined,
  map: undefined,
  strings: false,
  className: undefined,
};

export const SelectionBox = withStyles(styles)(SelectionBoxC);
