import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';

const styles = theme => ({
  actionButton: { position: 'relative' },
  cancelButton: {
    position: 'relative',
  },
  button: { margin: theme.spacing.unit, marginLeft: 0 },
  icon: { marginLeft: theme.spacing.unit },
});

const propTypes = {
  style: PropTypes.objectOf(PropTypes.any),
  firstButtonText: PropTypes.string.isRequired,
  secondButtonText: PropTypes.string.isRequired,
  editingOn: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  onEditingOn: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
};

const defaultProps = {
  style: {},
};

const EditCancelSaveButtons = ({
  editingOn,
  onSave,
  onCancel,
  onEditingOn,
  style,
  firstButtonText,
  secondButtonText,
  disabled,
  classes,
}) => (
  <div style={style}>
    <Button
      variant="contained"
      className={classes.button}
      color="primary"
      type="submit"
      onClick={editingOn ? onSave : onEditingOn}
      disabled={disabled}
    >
      {firstButtonText}
      <Icon className={classes.icon}>{editingOn ? 'save' : 'edit'}</Icon>
    </Button>
    {editingOn && (
      <Button variant="contained" className={classes.button} onClick={onCancel}>
        {secondButtonText}
        <Icon className={classes.icon}>clear</Icon>
      </Button>
    )}
  </div>
);

EditCancelSaveButtons.propTypes = propTypes;
EditCancelSaveButtons.defaultProps = defaultProps;

export default withStyles(styles)(EditCancelSaveButtons);
