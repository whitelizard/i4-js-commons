import React from 'react';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const propTypes = {
  title: PropTypes.string,
  topBar: PropTypes.node,
  noPaperBg: PropTypes.bool,
  children: PropTypes.node.isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  paperBgStyle: PropTypes.objectOf(PropTypes.any),
};
const defaultProps = {
  title: '',
  topBar: false,
  noPaperBg: false,
  paperBgStyle: {},
};

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
  bg: {
    width: '100vw',
    minHeight: '100vh',
    position: 'fixed',
    zIndex: -1,
  },
});

const hoc = withStyles(styles, { withTheme: true });

const PageC = ({ title, children, classes, noPaperBg, paperBgStyle, topBar }) => {
  return (
    <DocumentTitle title={title}>
      <>
        {!noPaperBg && <Paper className={classes.bg} style={paperBgStyle} />}
        {topBar}
        {topBar && <div className={classes.toolbar} />}
        {children}
      </>
    </DocumentTitle>
  );
};

PageC.propTypes = propTypes;
PageC.defaultProps = defaultProps;

export const Page = hoc(PageC);
