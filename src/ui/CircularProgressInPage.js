import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = () => ({
  worm: {
    position: 'absolute',
    zIndex: 2000,
    top: 250,
    left: '50%',
    marginTop: -50,
    marginLeft: -40,
  },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  color: PropTypes.string,
};
const defaultProps = {
  color: undefined,
};

const __CircularProgressInPage = ({ color, classes }) => (
  <CircularProgress style={{ color }} size={80} thickness={7} className={classes.worm} />
);

__CircularProgressInPage.propTypes = propTypes;
__CircularProgressInPage.defaultProps = defaultProps;

export const CircularProgressInPage = withStyles(styles)(__CircularProgressInPage);
