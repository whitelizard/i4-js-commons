import React from 'react';
import PropTypes from 'prop-types';
import 'mdi/css/materialdesignicons.min.css';
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';

const styles = () => ({});

const stringToThemeColor = (string, theme) => {
  if (string === 'inherit') return 'inherit';
  if (string === 'secondary') return theme.palette.secondary.main;
  if (string === 'primary') return theme.palette.primary.main;
  if (string === 'action') return theme.palette.action.active;
  if (string === 'disabled') return theme.palette.action.disabled;
  if (string === 'error') return theme.palette.error.main;
  return string;
};

const sizeToPix = {
  sm: '18px',
  md: '24px',
  lg: '36px',
  xl: '48px',
};
const sizeToPx = size => sizeToPix[size] || sizeToPix.md;

const propTypes = {
  children: PropTypes.node,
  theme: PropTypes.objectOf(PropTypes.any).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  icon: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string, // inherit, secondary, primary, action, disabled or error
};

const defaultProps = {
  children: undefined,
  color: undefined,
  className: '',
  size: 'md',
  style: {},
  icon: undefined,
};

class __FlexIcon extends React.PureComponent {
  render() {
    const { icon, style, theme, color, size, children, className } = this.props;
    // console.log('FLEXICON render:', icon, color, size, children);
    const iconStr = (children || icon).trim();
    const mdi = iconStr.startsWith('mdi-');
    const iconSize = typeof size === 'number' ? `${size}px` : sizeToPx(size);
    if (mdi) {
      return (
        <i
          style={{
            color: stringToThemeColor(color, theme),
            lineHeight: iconSize,
            height: iconSize,
            ...style,
          }}
          className={`mdi ${iconStr} mdi-${iconSize} ${className}`}
        />
      );
    }
    const inlineStyle = { fontSize: iconSize, ...style };
    if (color && (color.startsWith('#') || color.startsWith('rgb'))) {
      inlineStyle.color = color;
    }
    return (
      <Icon style={inlineStyle} color={color} className={className}>
        {iconStr}
      </Icon>
    );
  }
}

__FlexIcon.propTypes = propTypes;
__FlexIcon.defaultProps = defaultProps;

export const FlexIcon = withStyles(styles, { withTheme: true })(__FlexIcon);
