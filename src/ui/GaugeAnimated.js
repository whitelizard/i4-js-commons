import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createGauge } from 'gauge-animated/lib';

const propTypes = {
  value: PropTypes.number,
};

const defaultProps = {
  value: 0,
};

export const GaugeAnimated = props => {
  const { value } = props;
  const div = useRef();
  const gauge = useRef();

  useEffect(() => {
    gauge.current = createGauge(div.current, props);
    gauge.current.setTarget(value);
  }, []);

  useEffect(() => {
    gauge.current.setTarget(value);
  }, [value]);

  return <div ref={div} />;
};

GaugeAnimated.propTypes = propTypes;
GaugeAnimated.defaultProps = defaultProps;
