import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { unstable_useMediaQuery as useMediaQuery } from '@material-ui/core/useMediaQuery';
import { withTheme } from '@material-ui/core/styles';
import * as R from 'ramda';
import { Widget as WidgetFrame } from './Widget';

const defaultWidth = 300;
const wideWidth = 616;
const contentHeight = 332;

const ensureArray = R.when(R.complement(R.is)(Array), R.of);

const appliedArgToFuncReducer = (funcMap, argMap) => (hocs, key) =>
  R.append(funcMap[key](...ensureArray(argMap[key])))(hocs);

const propTypes = {
  dashboardConfig: PropTypes.objectOf(PropTypes.any),
  asset: PropTypes.objectOf(PropTypes.any).isRequired,
  theme: PropTypes.objectOf(PropTypes.any).isRequired,
  componentMap: PropTypes.objectOf(PropTypes.any).isRequired,
  widgetHocMap: PropTypes.objectOf(PropTypes.func),
};
const defaultProps = {
  dashboardConfig: {},
  widgetHocMap: {},
};

const hoc = withTheme();

const updateCache = (type, defaultOrder = [], currentOrder) => {
  if (!type) return undefined;
  const cache = JSON.parse(localStorage.getItem(`aT_${type}`)) || {};
  const version = JSON.stringify(defaultOrder);
  let data;
  if (cache.version !== version) data = { widgetOrder: defaultOrder, version };
  else data = { widgetOrder: currentOrder || cache.widgetOrder || defaultOrder, version };
  localStorage.setItem(`aT_${type}`, JSON.stringify(data));
  return data;
};

const DashboardGeneratorC = ({
  dashboardConfig: { widgets = [], widgetsInDashboard },
  asset: { type },
  theme,
  componentMap,
  widgetHocMap,
}) => {
  // console.log('DaGen props:', widgets, widgetsInDashboard, type, 'componentMap:', componentMap);

  const [widgetOrder, setWidgetOrder] = useState([]);
  const widgetComponent = useRef({});

  const updateWidgetOrder = currentOrder => {
    const data = updateCache(type, widgetsInDashboard || R.pluck('id')(widgets), currentOrder);
    setWidgetOrder(data.widgetOrder);
  };

  useEffect(() => {
    /* Clean up old items */
    const temporaryToRemoveOld = type && localStorage.getItem(type);
    if (temporaryToRemoveOld) {
      const order = JSON.parse(temporaryToRemoveOld).widgetOrder;
      if (order) localStorage.removeItem(type);
    }
    /* End of Clean up */
  }, []);

  useEffect(() => {
    updateWidgetOrder();
  }, [widgetsInDashboard, type, widgets]);

  const moveWidget = (id, step) => {
    const fromIndex = widgetOrder.indexOf(id);
    const newOrder = R.move(fromIndex, fromIndex + step)(widgetOrder);
    updateWidgetOrder(newOrder);
  };

  const getCommonMenuItems = id => [
    {
      label: 'Move left', // tLabel('moveLeft', lang)
      icon: 'mdi-arrow-top-left',
      method: () => moveWidget(id, -1),
    },
    {
      label: 'Move right', // tLabel('moveRight', lang)
      icon: 'mdi-arrow-bottom-right',
      method: () => moveWidget(id, 1),
    },
  ];

  const widgetBuilder = (widgetConf, dynWidth) => {
    const {
      id,
      hoc: wHoc,
      component,
      responsive,
      justify = 'center',
      align = 'center',
      contentProps = {},
      frameProps = {},
      ...rest
    } = widgetConf;
    if (!widgetComponent.current[id]) {
      const { [component]: Inner = R.F } = componentMap;
      const hocConfs = R.pick(Object.keys(widgetHocMap))(rest);
      // const hocFuncs = R.pick(Object.keys(rest))(widgetHocMap);
      const wHocs = R.compose(
        R.when(() => !!wHoc, R.append(wHoc)),
        R.reduce(appliedArgToFuncReducer(widgetHocMap, hocConfs), []),
        R.keys,
      )(hocConfs);
      const widgetHoc = R.isEmpty(wHocs) ? R.identity : R.compose(...wHocs);
      widgetComponent.current[id] = widgetHoc(Inner);
    }
    const Component = widgetComponent.current[id];
    const width = responsive ? dynWidth : defaultWidth;
    const { menuItems = [] } = frameProps;
    return (
      <WidgetFrame
        key={id}
        width={width}
        {...R.assoc('menuItems', R.concat(getCommonMenuItems(id), menuItems))(frameProps)}
        style={{
          marginTop: 16,
          marginLeft: 16,
        }}
      >
        <div style={{ display: 'flex', justifyContent: justify, alignItems: align, width: '100%' }}>
          <Component style={{ height: contentHeight, width }} {...contentProps} />
        </div>
      </WidgetFrame>
    );
  };

  const wide = useMediaQuery(theme.breakpoints.up('md'));
  const dynWidth = wide ? wideWidth : defaultWidth;
  // console.log('DaGen b4 render:', widgetOrder);
  return (
    <div>
      {R.map(id => {
        const conf = R.find(R.propEq('id', id))(widgets);
        // console.log('DaGen render widget:', conf);
        return conf && conf.component ? widgetBuilder(conf, dynWidth) : null;
      })(widgetOrder)}
    </div>
  );
};

DashboardGeneratorC.propTypes = propTypes;
DashboardGeneratorC.defaultProps = defaultProps;

export const DashboardGenerator = hoc(DashboardGeneratorC);
