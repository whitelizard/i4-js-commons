import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from '@material-ui/core/styles';
import { stringToElement } from 'gauge-animated/lib/gaugeface';
import { createGauge } from 'gauge-animated/lib';

function hexToRgbArray(hexStr) {
  // console.log('hexToRgbArray:', hexStr);
  return [0, 0, 0].map((v, i) =>
    parseInt((hexStr.startsWith('#') ? hexStr.slice(1) : hexStr).slice(i * 2, 2 * (i + 1)), 16),
  );
}

const rgbArrayToStr = rgb => `rgb(${rgb.join(',')})`;

const hexToRgbStr = x => rgbArrayToStr(hexToRgbArray(x));

const isEven = n => Math.abs(n % 2) === 0;

const limitMapper = (min, max, thresholds) => step =>
  thresholds[
    thresholds.reduce((res, limit, ix) => {
      // console.log('thresholds.reduce:', step, limit, ix, res);
      if (ix + 1 === thresholds.length && res === -1) return ix;
      const notALimit = isEven(ix);
      const valueFound = res > -1;
      const value = min + (step * (max - min)) / 100;
      const doContinue = value > limit;
      if (notALimit || valueFound || doContinue) {
        return res;
      }
      return ix - 1;
    }, -1)
  ];

const limitMapperToRgbStr = (min, max, thresholds) => {
  const mapper = limitMapper(min, max, thresholds);
  return step => hexToRgbStr(mapper(step));
};

const svgNeedle = size => {
  const c = 250;
  const scale = size / 500;
  return (0, stringToElement)(
    `<svg viewBox="0 0 ${size} ${size}" xmlns="http://www.w3.org/2000/svg" style="width:${size}px; height:${size}px;">\n
    <g>\n
      <g transform="scale(${scale})">\n
        <circle cx="${c}" cy="${c}" r="18" style="fill:#000"/>\n
        <path d="M ${c} ${c + 3.5} L ${c * 1.66} ${c + 2} L ${c * 1.6} ${c + 15} L ${c *
      1.9} ${c} L ${c * 1.6} ${c - 15} L ${c * 1.66} ${c - 2} L ${c} ${c -
      3.5} z" fill="#000" stroke="#111"/>\n
        <path d="M ${c} ${c + 4} L ${c - 75} ${c + 4} L ${c - 75} ${c - 4} L ${c} ${c -
      4} z" fill="#000" stroke="#111"/>\n
        <circle cx="${c - 80}" cy="${c}" r="17" style="fill:#000"/>\n
        <circle cx="${c - 80}" cy="${c}" r="6.5" style="fill:#fff"/>\n
        <circle cx="${c}" cy="${c}" r="4" style="stroke:#999;fill:#ccc"/>\n
      </g>\n
    </g>\n
    </svg>`,
  );
};

const hoc = withTheme();

const propTypes = {
  theme: PropTypes.objectOf(PropTypes.any).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  thresholds: PropTypes.arrayOf(PropTypes.any),
  // startColor: PropTypes.string,
  // endColor: PropTypes.string,
  size: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.number,
  markerColors: PropTypes.func,
  valueDisplayPostfix: PropTypes.string,
};

const defaultProps = {
  // startColor: 'ff0000',
  // endColor: '00ff00',
  size: 250,
  min: 0,
  max: 100,
  value: undefined,
  markerColors: undefined,
  valueDisplayPostfix: '',
  style: {},
  thresholds: ['#00dd80', 60, '#F7D802', 80, '#ff5630'],
};

const ThresholdGaugeC = ({
  min,
  max,
  thresholds,
  markerColors,
  theme,
  value,
  size,
  style,
  ...rest
}) => {
  const gauge = useRef();
  const div = useRef();

  useEffect(() => {
    const fontFamily = ' ';
    const options = {
      size,
      // fontFamily,
      // scaleLabelFontFamily: fontFamily,
      // faceTextFontFamily: fontFamily,
      valueDisplayFontFamily: fontFamily,
      startAngle: Math.PI * 0.6,
      stopAngle: Math.PI * 2.4,
      needleAngleMin: Math.PI * 0.5,
      needleAngleMax: Math.PI * 2.5,
      labelSteps: 0,
      stopPinColor: 0,
      markerWidth: size / 29,
      markerLength: size * 0.16,
      markerColors: markerColors || limitMapperToRgbStr(min, max, thresholds),
      valueDisplay: true,
      // valueDisplayRadius: 0,
      valueDisplayFontSize: size / 10,
      min,
      max,
      stepValue: (max - min) / 100,
      mediumSteps: 0,
      largeSteps: 0,
      needleSvg: svgNeedle,
      ...rest,
    };
    gauge.current = createGauge(div.current, options);
    gauge.current.setTarget(value);
  }, []);

  useEffect(() => {
    if (!gauge.current) return;
    gauge.current.setTarget(value);
  }, [value]);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        ...style,
      }}
    >
      <style>{`.gauge-value-display { font-size: ${size / 8}px }`}</style>
      <div ref={div} />
    </div>
  );
};

ThresholdGaugeC.propTypes = propTypes;
ThresholdGaugeC.defaultProps = defaultProps;

export const ThresholdGauge = hoc(ThresholdGaugeC);
