import React from 'react';
import PropTypes from 'prop-types';
import * as L from 'leaflet';
import * as R from 'ramda';
import { Map, TileLayer, Marker, Polyline } from 'react-leaflet';
// import iconImg from '../../images/place_black_1.png';
// import iconShadow from '../../images/marker-shadow.png';

const iconImg =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAQAAAD9CzEMAAABkElEQVR4Ae3SvWoUYRTG8f8kKCEk5C4CSaONH0WS1WqJX4V4AyKpBTXgHaiYQmwV8QIkxvRK1FQabbRa3SJaaCoxiYJD5HEKi+XwsnvOzGyxsP9f/z5vcRg2IGWcZoVNdsgL33nNXRpk1NIoS7RRwieuMELFZniHunjLNBU6wz7qYY9m+edz5JCXm5hhHzntMU2wUd6jgDeMEGoJBV0mUEYbBbXIcHcKJTxhjrHCPKsoYR53K8jiGp0tI4s7uNtM/N62hoxXuNtBxhy2BWR8w12OjDFs48j4g7vfpQZ+4e6r40IayNjG3QtkrGJbR8Zz3N1GWMt0dhNh3cJdEyWsscB4ocE6Smji7jA/UdAPDhHoMQp6RKiTKOgEwbZQwBbhLqGAi4TL+IicPpBRogvI6Twle4kcNijdUQ5QDwccoUL3UQ/3qNQkX1AX20xSsUXUxSI19BCR9oBamqCNEj4zQU0dJ0dGzjFq7AYyrlNrGc9Qh6fU3hQt9F+LKfrQLLsIscssfeocfwtn6WNXC8MGrH8b0qYwWjeMJwAAAABJRU5ErkJggg==';
const iconShadow =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAQAAAACach9AAACJ0lEQVR4Ae3ShY6bQRADYIeZ3v+9yszMDZPlkSWtslPuiS6+C+8n/zNp4p/froi8Jq/Jxk+TkwmHX//Wz8lG3jNjTeaguabD6HAVbphMwIIzqtd+rzKYhsgUNNcqo3fEBlyyOWmwhbbS4b8SaLBu7SGlpL9gklw3YpYR6J5Bm0zAguspZjvqSVQxqvxsPSJJKYIFKh5A2VIDyMgYfYB9hvcmI0VPTxVZS81ER7qiBoFqBAa9MKOKLzzv2BE3EOnlmDTrNRXryToGOGR65nhvSphjMP6b6WJ6xMYYxSW7l1Pp6CSzbLFPgAN3NGLKPRn3c59qxzapCTPUFNVKh0vUe24azMkW+uRm/B8UU3O3Eiw4nOrrafOSF5gS7PqHUotBQQ6f11p22HDOi+66i2LIHcWJOZrTP+rkhOl7n8YcTzCQIuIvyZPQrvs5Br0OdTsyB+YYrwKokXvsgOIXZzbeQ0DMnhHIqGOdBMEl9qKYAo3pBbbDNkijUSgjv/PrMFhwRwS2YbbMzqg+SzdukoxbeiSEVlgyK6yFmjSYtDxhTXRv1OsgsCb2jflu8hLMyC0P7Qg54NE1qc/MF5PqeAHWSfDL3zxN9dsQ+YD3zEeRS4GeYgFm5I7HNiJPfP4V7/CKeUvwsxpqhkwFzMgjj33Vwe+EnuIxngn8FvNTSCkwmJG+rQm8J/MId/EQL3nZwe2EmSNm9ufkFq9xHzcJPscnLGMRomBKqdzOY2EH+IPOLMEAAAAASUVORK5CYII=';

const defaultCenter = [57, 16];
const iconCommon = {
  iconAnchor: [24, 43],
  iconSize: [48, 48],
  shadowUrl: iconShadow,
  shadowAnchor: [13, 39],
  shadowSize: [41, 41],
  popupAnchor: [0, -50],
};
const posIcon = L.icon({ iconUrl: iconImg, ...iconCommon });

export const PositionMap = ({ style, position, history }) => {
  if (!position || R.isEmpty(position)) {
    return (
      <Map style={style} center={defaultCenter} zoom={2} maxZoom={19} minZoom={2}>
        <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
      </Map>
    );
  }
  const pos = position;
  const positions = history ? [...history, position] : [pos];
  const points = positions.map(p => L.latLng(p));
  return (
    <Map style={style} center={pos} zoom={12} maxZoom={19} minZoom={2}>
      <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
      <Marker markerZoomAnimation={false} position={points[points.length - 1]} icon={posIcon} />
      {history && <Polyline positions={points} />}
    </Map>
  );
};

PositionMap.propTypes = {
  style: PropTypes.objectOf(PropTypes.any),
  position: PropTypes.arrayOf(PropTypes.number),
  history: PropTypes.arrayOf(PropTypes.array),
};

PositionMap.defaultProps = {
  style: {},
  history: undefined,
  position: undefined,
};
