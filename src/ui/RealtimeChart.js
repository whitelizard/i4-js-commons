import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import ChartJS from 'chart.js';
import * as R from 'ramda';
import { nilOrEmpty } from '../transforms';

const propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  type: PropTypes.string,
  options: PropTypes.objectOf(PropTypes.any),
  payload: PropTypes.arrayOf(PropTypes.number), // eslint-disable-line
  length: PropTypes.number, // eslint-disable-line
};
const defaultProps = {
  style: {},
  type: 'line',
  options: {},
  payload: [],
  length: 10,
};

const pointRadius = 0;
const chartColors = [
  'rgb(50, 152, 235)',
  'rgb(255, 159, 64)',
  'rgb(75, 185, 92)',
  'rgb(153, 102, 255)',
  'rgb(255, 99, 99)',
  'rgb(255, 205, 86)',
  'rgb(190, 90, 60)',
  'rgb(231, 80, 200)',
  'rgb(60, 203, 180)',
  'rgb(255, 20, 0)',
  'rgb(201, 203, 207)',
];
const defaultCanvasStyle = { width: 300, height: 300 };
const commonOptions = {
  animation: { duration: 200 },
  legend: {
    labels: {
      boxWidth: 6,
    },
  },
};
const defaultOptions = {
  bar: {
    ...commonOptions,
  },
  line: {
    ...commonOptions,
    tooltips: { enabled: false },
    scales: {
      xAxes: [
        {
          type: 'time',
          time: {
            parser: 'mm:ss',
            tooltipFormat: 'mm:ss',
            minUnit: 'second',
            displayFormats: { second: 'mm:ss' },
          },
          scaleLabel: {
            display: false,
            // labelString: 'Date',
          },
        },
      ],
      yAxes: [
        {
          scaleLabel: {
            display: false,
            // labelString: 'value',
          },
        },
      ],
    },
  },
};

const payloadIntoChart = R.curry((chart, length, payload) => {
  const dataArrays = [chart.data.labels, ...R.pluck('data', chart.data.datasets)];
  let remove = true;
  if (payload[0] > dataArrays[0][0] && dataArrays[0].length < length) {
    remove = false;
  }
  dataArrays.forEach((da, ix) => {
    da.push(payload[ix]);
    if (remove) da.shift();
  });
});

export const RealtimeChart = ({ style, type, data, options, payload, length, ...rest }) => {
  const canvasRef = useRef(null);
  const chart = useRef(null);

  useEffect(() => {
    // console.log('Chart init:', type, JSON.stringify(data));
    if (!chart.current) {
      /* eslint-disable no-param-reassign */
      data.datasets.forEach((ds, ix) => {
        if (!ds.backgroundColor) ds.backgroundColor = chartColors[ix];
        if (!ds.borderColor) ds.borderColor = chartColors[ix];
        if (!ds.pointRadius) ds.pointRadius = pointRadius;
      });
      /* eslint-enable no-param-reassign */
      chart.current = new ChartJS(canvasRef.current, {
        type,
        data,
        options: R.mergeDeepLeft(options, defaultOptions[type]),
      });
    }
  }, []);

  useEffect(() => {
    if (nilOrEmpty(payload)) return;
    // console.log('Chart update:', payload);
    if (length === 0) {
      // console.log('length is 0: payload -> data');
      if (Array.isArray(payload[0])) {
        // Support multiple datasets (?)
      } else {
        chart.current.data.datasets[0].data = payload;
      }
    } else if (Array.isArray(payload[0])) {
      R.map(payloadIntoChart(chart.current, length))(payload);
    } else {
      payloadIntoChart(chart.current, length)(payload);
    }
    chart.current.update();
  }, [payload]);

  const canvasStyle = { ...defaultCanvasStyle, ...style };
  // canvasStyle.minHeight = canvasStyle.height;
  return (
    <canvas
      ref={canvasRef}
      width={canvasStyle.width}
      height={canvasStyle.height}
      style={canvasStyle}
      {...rest}
    />
  );
};

RealtimeChart.propTypes = propTypes;
RealtimeChart.defaultProps = defaultProps;
