import React, { useRef, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import ChartJS from 'chart.js';
import * as R from 'ramda';
import { nilOrEmpty } from '../transforms';
// import { generateString } from '../data-utils';

const propTypes = {
  data: PropTypes.objectOf(PropTypes.any),
  style: PropTypes.objectOf(PropTypes.any),
  type: PropTypes.string,
  options: PropTypes.objectOf(PropTypes.any),
  values: PropTypes.arrayOf(PropTypes.any),
  point: PropTypes.objectOf(PropTypes.any),
  length: PropTypes.number,
};
const defaultProps = {
  style: {},
  data: undefined,
  values: undefined,
  type: 'line',
  options: {},
  point: {},
  length: 10,
};

const pointRadius = 1.3;
const chartColors = [
  'rgb(50, 152, 235)',
  'rgb(255, 159, 64)',
  'rgb(75, 185, 92)',
  'rgb(153, 102, 255)',
  'rgb(255, 99, 99)',
  'rgb(255, 205, 86)',
  'rgb(190, 90, 60)',
  'rgb(231, 80, 200)',
  'rgb(60, 203, 180)',
  'rgb(255, 20, 0)',
  'rgb(201, 203, 207)',
];
const defaultCanvasStyle = { width: 300, height: 300 };
const commonOptions = {
  animation: { duration: 200 },
  legend: {
    labels: {
      boxWidth: 6,
    },
  },
};
const getDefaultDataset = (idx, data = []) => ({
  fill: false,
  label: `pl[${idx}]`,
  data,
  backgroundColor: chartColors[idx],
  borderColor: chartColors[idx],
  pointRadius,
});
const defaultOptions = {
  bar: {
    ...commonOptions,
  },
  line: {
    ...commonOptions,
    // tooltips: { enabled: false },
    scales: {
      xAxes: [
        {
          type: 'time',
          time: {
            parser: 'mm:ss',
            tooltipFormat: 'mm:ss',
            minUnit: 'second',
            displayFormats: { second: 'mm:ss' },
          },
          scaleLabel: {
            display: false,
            // labelString: 'Date',
          },
        },
      ],
      yAxes: [
        {
          scaleLabel: {
            display: false,
            // labelString: 'value',
          },
        },
      ],
    },
  },
};

const adjustDatasetsToPayload = (datasets, payload) => {
  while (datasets.length < payload.length) {
    datasets.push(
      getDefaultDataset(datasets.length, R.repeat(null, datasets[0] ? datasets[0].data.length : 0)),
    );
  }
  while (datasets.length > payload.length) {
    datasets.pop();
  }
};

const pointIntoChart = (chart, length, point) => {
  const x = point.x || point.ts || point.timestamp || point.time || new Date();
  const ys = point.payload || point.pl || point.data || [null];
  // Add or remove datasets depending on number of ys:
  adjustDatasetsToPayload(chart.data.datasets, ys);
  // chart.update();
  // Calculate if we should cut the tail
  let remove = true;
  const chronologic = nilOrEmpty(chart.data.labels) || x > chart.data.labels[0];
  if (chronologic && chart.data.labels.length < length) remove = false;
  // Add the new data point (to labels & datasets)
  if (remove) chart.data.labels.shift();
  chart.data.labels.push(x);
  chart.data.datasets.forEach((ds, ix) => {
    const val = ys[ix];
    // if (!R.is(Object, val) && !R.is(Number, val) && !R.isNil(val)) val = { data: val };
    // if (R.is(Object, val)) val.valueOf = () => 0;
    ds.data.push(val);
    if (remove) ds.data.shift();
  });
};

const fixDatasetStyle = (ds, ix) => {
  /* eslint-disable no-param-reassign */
  R.mapObjIndexed((val, key) => {
    ds[key] = ds[key] || val;
  })(getDefaultDataset(ix, ds.data));
  // if (!ds.backgroundColor) ds.backgroundColor = chartColors[ix];
  // if (!ds.borderColor) ds.borderColor = chartColors[ix];
  // if (!ds.pointRadius) ds.pointRadius = pointRadius;
  /* eslint-enable no-param-reassign */
};

export const Chart = ({ style, type, data, options, point, length, values, ...rest }) => {
  const canvasRef = useRef(null);
  const chart = useRef(null);
  // const chartId = useRef(generateString(8));

  useEffect(() => {
    // console.log('Chart init:', type, data);
    if (!chart.current) {
      /* eslint-disable no-param-reassign */
      data = data || {};
      if (!Array.isArray(data.datasets)) data.datasets = [];
      /* eslint-enable no-param-reassign */
      data.datasets.forEach(fixDatasetStyle);
      chart.current = new ChartJS(canvasRef.current, {
        type,
        data,
        options: R.mergeDeepLeft(options, defaultOptions[type]),
      });
    }
  }, []);

  useEffect(() => {
    // console.log('Chart update:', length, point);
    if (nilOrEmpty(point)) return;
    // if (Array.isArray(point)) {
    //   R.map(payloadIntoChart(chart.current, length))(payload);
    pointIntoChart(chart.current, length, point);
    // console.log('Chart datasets:', chart.current.data.datasets);
    // console.log('Chart labels:', chart.current.data.labels);
    chart.current.update();
  }, [point]);

  useEffect(() => {
    if (nilOrEmpty(values)) return;
    chart.current.data.datasets[0].data = values;
  }, [values]);

  const canvasStyle = { ...defaultCanvasStyle, ...style };
  // canvasStyle.minHeight = canvasStyle.height;
  return useMemo(() => {
    return (
      <canvas
        // id={chartId}
        ref={canvasRef}
        width={canvasStyle.width}
        height={canvasStyle.height}
        style={canvasStyle}
        {...rest}
      />
    );
  }, [chart.current]);
};

Chart.propTypes = propTypes;
Chart.defaultProps = defaultProps;
