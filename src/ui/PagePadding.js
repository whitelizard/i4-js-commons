import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  paddingT: {
    paddingTop: theme.spacing.unit * 2,
    [theme.breakpoints.up('sm')]: { paddingTop: theme.spacing.unit * 3 },
  },
  paddingB: {
    paddingBottom: 100,
  },
  paddingH: {
    paddingLeft: theme.spacing.unit * 1,
    paddingRight: theme.spacing.unit * 1,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3,
    },
  },
});

const propTypes = {
  children: PropTypes.node.isRequired,
  noHoriz: PropTypes.bool,
  noBot: PropTypes.bool,
  noTop: PropTypes.bool,
  className: PropTypes.string,
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
};
const defaultProps = {
  noHoriz: false,
  noBot: false,
  noTop: false,
  className: undefined,
  style: {},
};

const PagePaddingC = ({ noHoriz, noBot, noTop, classes, children, style, className }) => {
  return (
    <div
      className={classnames({
        [className]: !!className,
        [classes.paddingT]: !noTop,
        [classes.paddingB]: !noBot,
        [classes.paddingH]: !noHoriz,
      })}
      style={style}
    >
      {children}
    </div>
  );
};

PagePaddingC.propTypes = propTypes;
PagePaddingC.defaultProps = defaultProps;

export const PagePadding = withStyles(styles)(PagePaddingC);
