import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import ChartJS from 'chart.js';
import { withStyles } from '@material-ui/core/styles';
import * as R from 'ramda';

export const pointsToChartData = (label, points = []) => {
  /* eslint-disable no-param-reassign */
  if (!points.length) points = [{ ts: new Date(), pl: [] }];
  /* eslint-enable no-param-reassign */
  const datasets = [];
  const [{ pl }] = points;
  pl.forEach((_, ix) => {
    datasets.push({
      label: pl.length === 1 ? label : `${label}-${ix}`,
      data: R.compose(
        R.pluck(ix),
        R.pluck('pl'),
      )(points),
    });
  });
  return { labels: R.pluck('ts')(points), datasets };
};

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  points: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  type: PropTypes.string,
  title: PropTypes.string,
  options: PropTypes.objectOf(PropTypes.any),
};
const defaultProps = {
  style: {},
  type: 'line',
  title: 'noname',
  options: {},
};

const pointRadius = 1.5;
const fill = false;
const borderWidth = 1;
const chartColors = [
  'rgb(50, 152, 235)',
  'rgb(255, 159, 64)',
  'rgb(75, 185, 92)',
  'rgb(153, 102, 255)',
  'rgb(255, 99, 99)',
  'rgb(255, 205, 86)',
  'rgb(190, 90, 60)',
  'rgb(231, 80, 200)',
  'rgb(60, 203, 180)',
  'rgb(255, 20, 0)',
  'rgb(201, 203, 207)',
];
const defaultCanvasStyle = { width: 300, height: 640 };
const commonOptions = {
  // animation: { duration: 200 },
  legend: { labels: { boxWidth: 6 } },
  maintainAspectRatio: false,
};
const defaultOptions = {
  // bar: {
  //   ...commonOptions,
  // },
  line: {
    ...commonOptions,
    // tooltips: { enabled: false },
    scales: {
      xAxes: [
        {
          type: 'time',
          time: {
            // parser: 'mm:ss',
            tooltipFormat: 'D MMM H:mm:ss.SSS',
            minUnit: 'second',
            displayFormats: {
              millisecond: 'H:mm:ss.SSS',
              second: 'H:mm:ss',
              minute: 'H:mm',
              hour: 'H',
            },
          },
          // scaleLabel: {
          //   // display: false,
          //   // labelString: 'Date',
          // },
        },
      ],
      yAxes: [
        // {
        //   scaleLabel: {
        //     // display: false,
        //     // labelString: 'value',
        //   },
        // },
      ],
    },
  },
};
const styles = () => ({
  cointainer: {
    position: 'relative',
    margin: 'auto',
    // width: '100vw',
  },
});

const __StaticChart = ({ style, type, title, points, classes, options, ...rest }) => {
  const canvasRef = useRef(null);
  const chart = useRef(null);

  useEffect(() => {
    // console.log('Chart init:', type, JSON.stringify(points));
    const data = pointsToChartData(title, points);
    /* eslint-disable no-param-reassign */
    data.datasets.forEach((ds, ix) => {
      if (!ds.backgroundColor) ds.backgroundColor = chartColors[ix];
      if (!ds.borderColor) ds.borderColor = chartColors[ix];
      if (!ds.pointRadius) ds.pointRadius = pointRadius;
      if (!ds.fill) ds.fill = fill;
      if (!ds.borderWidth) ds.borderWidth = borderWidth;
    });
    /* eslint-enable no-param-reassign */
    chart.current = new ChartJS(canvasRef.current, {
      type,
      data,
      options: R.mergeDeepLeft(options, defaultOptions[type]),
    });
  }, [points]);

  const canvasStyle = { ...defaultCanvasStyle, ...style };
  return (
    <div className={classes.container}>
      <canvas
        ref={canvasRef}
        width={canvasStyle.width}
        height={canvasStyle.height}
        style={canvasStyle}
        {...rest}
      />
    </div>
  );
};

__StaticChart.propTypes = propTypes;
__StaticChart.defaultProps = defaultProps;

export const StaticChart = withStyles(styles)(__StaticChart);
