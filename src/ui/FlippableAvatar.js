import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  checkicon: { backgroundColor: 'green' },
  flipContainer: {
    perspective: 1000,
    height: 40,
    width: 40,
    backfaceVisibility: 'hidden',
    transition: '0.6s',
    transformStyle: 'preserve-3d',
    position: 'relative',
  },
  hover: { transform: 'scale(1.1)' },
  flipped: {
    transform: 'rotateY(180deg)',
  },
  back: {
    transform: 'rotateY(180deg)',
    position: 'absolute',
  },
  front: {
    position: 'absolute',
  },
  tooltip: { width: 64, height: 64, position: 'absolute', top: 0, left: 0 },
});

const propTypes = {
  style: PropTypes.objectOf(PropTypes.any),
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  src: PropTypes.string,
  tooltipText: PropTypes.string,
  onChange: PropTypes.func,
};

const defaultProps = {
  style: {},
  src: 'A',
  onChange: () => {},
  tooltipText: undefined,
};

const __FlippableAvatar = ({ style, classes, src, tooltipText, onChange }) => {
  const [flipped, setFlipped] = useState(false);

  const flip = ev => {
    ev.stopPropagation();
    onChange(!flipped);
    setFlipped(!flipped);
  };

  const text = src.length < 3 ? src : undefined;
  const image = src.length > 3 ? src : undefined;
  // console.log('tooltipText in FlippableAvatar()', tooltipText);
  return (
    <div style={style} onClick={flip}>
      <div
        id="flipcontainer"
        className={classnames(classes.flipContainer, flipped && classes.flipped)}
        onClick={flip}
        role="status"
      >
        <Avatar className={classes.back} classes={{ root: classes.checkicon }}>
          <Icon>check</Icon>
        </Avatar>
        <Avatar src={image} className={classes.front}>
          {text}
        </Avatar>
      </div>
      {tooltipText && (
        <Tooltip id="avatar-tooltip" title={tooltipText} placement="right">
          <div className={classes.tooltip} />
        </Tooltip>
      )}
    </div>
  );
};

__FlippableAvatar.propTypes = propTypes;
__FlippableAvatar.defaultProps = defaultProps;

export const FlippableAvatar = withStyles(styles)(__FlippableAvatar);
