import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  content: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
};

const styles = theme => ({
  contentElement: {
    float: 'left',
    marginRight: theme.spacing.unit * 4,
    marginBottom: theme.spacing.unit * 2,
    borderLeft: `solid 2px ${theme.palette.divider}`,
    paddingLeft: theme.spacing.unit,
    '&:last-child': { marginRight: 0 },
    [theme.breakpoints.up('md')]: { marginRight: theme.spacing.unit * 8 },
  },
});

const __HorizontalTableCell = ({ classes, label, content }) => (
  <div className={classes.contentElement}>
    {typeof label === 'string' ? <Typography>{label}</Typography> : label}
    {typeof content === 'string' ? <Typography variant="Title">{content}</Typography> : content}
  </div>
);

__HorizontalTableCell.propTypes = propTypes;

export const HorizontalTableCell = withStyles(styles)(__HorizontalTableCell);
