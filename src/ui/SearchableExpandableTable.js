import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import * as R from 'ramda';
import { FlippableAvatar } from './FlippableAvatar';

const styles = theme => ({
  container: { position: 'relative' },
  flexContainer: { justify: 'start', align: 'center', flex: 1 },
  tools: { align: 'center', flex: 2 },
  expandHeader: { lineHeight: '40px' },
  expandHeaderHover: {
    '&:hover': { background: theme.palette.action.hover },
  },
  searchField: { align: 'start', flex: 3, paddingLeft: theme.spacing.unit * 3, paddingBottom: 0 },
  tooltipContainer: {
    position: 'absolute',
    right: 48,
    top: 8,
  },
  tooltipBody: {
    maxWidth: 180,
  },
  selectComponent: { [theme.breakpoints.up('md')]: { paddingLeft: theme.spacing.unit * 7 } },
  expandIcon: {
    transform: 'rotate(0deg) translateY(-50%) !important',
  },
  actionsPanel: {
    justifyContent: 'center',
  },
  primaryAction: {
    display: 'block',
    position: 'absolute',
    width: 64,
    height: 64,
    right: 0,
    top: 0,
    cursor: 'pointer',
  },
  heading: { color: theme.palette.text.hint },
  headings: {
    display: 'flex',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 7}px ${theme.spacing.unit}px`,
  },
  content: { display: 'block' },
  checkboxes: { height: 20, width: 20, paddingRight: theme.spacing.unit * 3 },
  avatar: { padding: 0, paddingRight: 16 },
});

const iconSizeCompensation = 32;

const filterHelper = (array, searchText) => {
  if (!searchText) return true;
  for (let i = 0; i < array.length; i += 1) {
    const value = array[i].toString();
    // console.log('filterHelper value:', value, searchText);
    if (value && value.toLowerCase().includes(searchText.toLowerCase())) {
      return true;
    }
  }
  return false;
};

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  theme: PropTypes.objectOf(PropTypes.any).isRequired,
  // width: PropTypes.string,
  // windowWidth: PropTypes.number.isRequired,
  selectComponent: PropTypes.node,
  onSelected: PropTypes.func,
  data: PropTypes.objectOf(PropTypes.any),
  headings: PropTypes.arrayOf(PropTypes.string).isRequired,
  iconTooltip: PropTypes.string,
  searchFieldHintText: PropTypes.string,
  avatar: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  selectable: PropTypes.bool,
  preSelected: PropTypes.objectOf(PropTypes.bool),
  useAsTable: PropTypes.bool,
  showConnectionStatuses: PropTypes.bool,
};

const defaultProps = {
  data: {},
  // width: undefined,
  selectable: false,
  useAsTable: false,
  selectComponent: undefined,
  onSelected: () => {},
  searchFieldHintText: 'Search',
  iconTooltip: '',
  avatar: false,
  preSelected: undefined,
  showConnectionStatuses: false,
};

class SearchableExpandableTableC extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      checked: props.preSelected || {},
    };
  }

  getCheckboxComponent = obj => {
    const { classes, avatar, iconTooltip } = this.props;
    const { checked } = this.state;
    const objRid = obj.rid;
    const objAvatar = obj.avatar || avatar;
    return avatar ? (
      <FlippableAvatar
        src={objAvatar}
        style={{ padding: 0, paddingRight: 16 }}
        onChange={this.handleChecked(objRid)}
        tooltipText={iconTooltip}
      />
    ) : (
      <Checkbox
        className={classes.checkboxes}
        checked={checked[objRid] || false}
        onClick={ev => ev.stopPropagation()}
        onChange={(ev, isChecked) => this.handleChecked(objRid)(isChecked)}
      />
    );
  };

  createConnectionIndicator = (connectionStatus, size = 20) => {
    const fillColor = {
      true: 'lightgreen',
      false: 'red',
      undefined: 'gray',
    }[connectionStatus];
    // const fillColor = R.cond([
    //   [R.equals(true), R.always('lightgreen')],
    //   [R.equals(false), R.always('red')],
    //   [R.T, R.always('gray')],
    // ])(connectionStatus);
    return (
      <svg height={size} width={size}>
        <circle
          cx={size / 2}
          cy={size / 2}
          r="6"
          stroke="black"
          strokeWidth="0.5"
          fill={fillColor}
        />
      </svg>
    );
  };

  createAvatar = objAvatar => {
    const { avatar, classes, iconTooltip } = this.props;
    let src;
    let text;
    if (typeof avatar === 'boolean') {
      text = objAvatar.length < 3 ? objAvatar : undefined;
      src = objAvatar.length > 3 ? objAvatar : undefined;
    } else {
      text = avatar.length < 3 ? avatar : undefined;
      src = avatar.length > 3 ? avatar : undefined;
    }
    return (
      <Tooltip id={`tooltip${objAvatar}`} title={iconTooltip} placement="bottom-start">
        <div className={classes.avatar}>
          <Avatar src={src}>{text}</Avatar>
        </div>
      </Tooltip>
    );
  };

  handleSearchText = ev => this.setState({ searchText: ev.target.value });

  handlePanelChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleChecked = rid => isChecked => {
    const { checked } = this.state;
    const { onSelected } = this.props;
    // console.log('handlechecked', checked, rid, isChecked);
    const newChecked = { ...checked, [rid]: isChecked };
    this.setState({ checked: newChecked });
    onSelected(Object.keys(R.filter(v => v, newChecked)));
  };

  render() {
    const {
      classes,
      theme,
      // windowWidth,
      data,
      searchFieldHintText,
      headings,
      selectComponent,
      selectable,
      useAsTable,
      avatar,
      showConnectionStatuses,
    } = this.props;
    const { expanded, checked, searchText } = this.state;
    const filteredObjects = R.filter(obj => filterHelper(obj.values, searchText), data);
    // TODO: no container anymore, make new API for small screen adaptation
    const smallScreen = theme.breakpoints.down('md');
    // console.log('SearchableExpandableTable:', windowWidth, theme.breakpoints.values.md);
    const topLevelHeadings = smallScreen ? headings.slice(0, 2) : headings.slice(0, 4);
    const detailHeadings = smallScreen
      ? headings.slice(2, headings.length)
      : headings.slice(4, headings.length);
    let firstColumnPadding = theme.spacing.unit * 3;
    if (selectable) firstColumnPadding = theme.spacing.unit * 6 + iconSizeCompensation - 12;
    if (avatar) firstColumnPadding = theme.spacing.unit * 6 + iconSizeCompensation;
    return (
      <div className={classes.container}>
        <div className={classes.flexContainer}>
          <div className={classes.searchField}>
            <TextField id="search" label={searchFieldHintText} onChange={this.handleSearchText} />
          </div>
          <div className={classes.tools}>
            {R.any(v => v, checked) && (
              <div className={classes.selectComponent}>{selectComponent}</div>
            )}
          </div>
        </div>
        <div className={classes.headings} style={{ paddingLeft: firstColumnPadding }}>
          {topLevelHeadings.map((title, i) => (
            <div key={title || i} style={{ flexBasis: `${100 / topLevelHeadings.length}%` }}>
              <Typography className={classes.heading}>{title}</Typography>
            </div>
          ))}
        </div>
        {Object.values(filteredObjects).map(obj => {
          const objRid = obj.rid;
          const { connectionStatus = undefined } = obj;
          const isExpanded = expanded === objRid;
          const primaryAction =
            (obj.actions && obj.actions[Object.keys(obj.actions)[0]]) ||
            (selectable && {
              action: () => this.handleChecked(objRid)(!checked.objRid || false),
            });
          const topLevelValues = smallScreen
            ? R.slice(0, 2, obj.values)
            : R.slice(0, 4, obj.values);
          const detailValues = R.drop(smallScreen ? 2 : 4, obj.values);
          return (
            <ExpansionPanel
              key={objRid}
              expanded={isExpanded}
              onChange={
                useAsTable ? primaryAction && primaryAction.action : this.handlePanelChange(objRid)
              }
            >
              {primaryAction && (
                <link className={classes.primaryAction} onClick={primaryAction.action} />
              )}
              {primaryAction && primaryAction.tooltip && (
                <Tooltip id={`tooltip${objRid}`} title={primaryAction.tooltip} placement="right">
                  <link className={classes.primaryAction} onClick={primaryAction.action} />
                </Tooltip>
              )}
              <ExpansionPanelSummary
                className={classnames(useAsTable && classes.expandHeaderHover)}
                classes={{
                  expandIcon:
                    primaryAction && primaryAction.icon && isExpanded && classes.expandIcon,
                }}
                expandIcon={
                  !useAsTable &&
                  ((primaryAction && primaryAction.icon && <Icon>{primaryAction.icon}</Icon>) || (
                    <Icon>expand_more</Icon>
                  ))
                }
              >
                {showConnectionStatuses && this.createConnectionIndicator(connectionStatus)}
                {selectable && this.getCheckboxComponent(obj)}
                {!selectable && avatar && this.createAvatar(obj.avatar)}
                {topLevelValues.map((tlv, index) => (
                  <div
                    // eslint-disable-next-line
                    key={`${index}/${tlv}`}
                    style={{ flexBasis: `${100 / topLevelHeadings.length}%` }}
                  >
                    <Typography className={avatar ? classes.expandHeader : undefined}>
                      {tlv}
                    </Typography>
                  </div>
                ))}
              </ExpansionPanelSummary>
              {detailValues.length > 0 && (
                <ExpansionPanelDetails>
                  {detailValues.map((dval, i) => (
                    <div key={dval} style={{ flexBasis: '33.3%' }}>
                      <Typography>{`${detailHeadings[i]}: ${dval}`}</Typography>
                    </div>
                  ))}
                </ExpansionPanelDetails>
              )}
              {obj.content && (
                <ExpansionPanelDetails>
                  <div className={classes.content}>{obj.content(obj)}</div>
                </ExpansionPanelDetails>
              )}
              {obj.actions && <Divider />}
              {obj.actions && (
                <ExpansionPanelActions classes={{ root: classes.actionsPanel }}>
                  {Object.values(obj.actions).map(action => (
                    <BottomNavigationAction
                      key={action.label}
                      showLabel
                      onClick={action.action}
                      label={action.label}
                      icon={<Icon>{action.icon}</Icon>}
                    />
                  ))}
                </ExpansionPanelActions>
              )}
            </ExpansionPanel>
          );
        })}
      </div>
    );
  }
}

SearchableExpandableTableC.propTypes = propTypes;
SearchableExpandableTableC.defaultProps = defaultProps;

export const SearchableExpandableTable = withStyles(styles, { withWidth: true, withTheme: true })(
  SearchableExpandableTableC,
);
