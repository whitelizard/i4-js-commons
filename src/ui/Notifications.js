import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import * as R from 'ramda';
import { format } from 'date-fns/fp';
import { tsToDate, dateToTs } from '../dm-utils';

const severityMap = {
  none: { icon: 'chat_bubble_outline' },
  minor: { icon: 'announcement' },
  major: { icon: 'warning', color: 'orange' },
  critical: { icon: 'report', color: 'red' },
  success: { icon: 'chat_bubble_outline' },
  info: { icon: 'announcement' },
  warning: { icon: 'warning', color: 'orange' },
  error: { icon: 'report', color: 'red' },
};

const styles = theme => ({
  wrapper: { overflowY: 'auto', overflowX: 'hidden', height: '100%' },
  icon: { alignSelf: 'center', paddingRight: theme.spacing.unit * 2 },
  text: { paddingLeft: 12 },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  messages: PropTypes.arrayOf(PropTypes.any),
  style: PropTypes.objectOf(PropTypes.any),
};
const defaultProps = {
  style: {},
  messages: [],
};

const Notification = ({ classes, msg }) => {
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    setTimeout(() => setExpanded(true), 10);
  }, []);

  const { ts = dateToTs(new Date()), pl = [{}] } = msg;
  const { signal = 'Unknown', severity = 'none' } = pl[0];
  const iConf = severityMap[severity] || severityMap.none;
  return (
    <Collapse in={expanded}>
      <div style={{ display: 'flex', padding: '12px 6px 12px 12px' }}>
        {
          <Icon className={classes.icon} style={iConf.color && { color: iConf.color }}>
            {iConf.icon}
          </Icon>
        }
        <div className={classes.text}>
          <Typography>{signal}</Typography>
          <Typography variant="caption">{format('d MMM HH:mm')(tsToDate(ts))}</Typography>
        </div>
      </div>
    </Collapse>
  );
};

Notification.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  msg: PropTypes.objectOf(PropTypes.any).isRequired,
};

const NotificationsC = ({ style, messages, classes }) => (
  // console.log('Notifications', messages);

  <div style={style} className={classes.wrapper}>
    <Divider />
    {R.compose(
      R.map(msg => (
        <div key={msg.ts}>
          <Notification classes={classes} msg={msg} />
          <Divider />
        </div>
      )),
      R.reverse,
      R.when(m => !Array.isArray(m), R.of),
    )(messages)}
  </div>
);

NotificationsC.propTypes = propTypes;
NotificationsC.defaultProps = defaultProps;

const hoc = withStyles(styles);

export const Notifications = hoc(NotificationsC);
