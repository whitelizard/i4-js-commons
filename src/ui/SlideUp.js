import React from 'react';
import Slide from '@material-ui/core/Slide';

export const SlideUp = props => <Slide direction="up" {...props} />;
