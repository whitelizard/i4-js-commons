import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { FlexIcon } from './FlexIcon';

export const okColor = '#00dd80';

const styles = () => ({
  '@keyframes shake': {
    '0%': { transform: 'rotate(-5deg)' },
    '50%': { transform: 'rotate(3deg)' },
  },
  alarm: {
    animation: 'shake .2s linear infinite',
  },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  signal: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  size: PropTypes.number,
  animatedErr: PropTypes.bool,
  className: PropTypes.string,
  okIconId: PropTypes.string,
  errIconId: PropTypes.string,
  iconStyle: PropTypes.objectOf(PropTypes.any),
};

const defaultProps = {
  signal: 1,
  iconStyle: {},
  animatedErr: true,
  size: 56,
  errIconId: 'warning',
  okIconId: 'check',
  className: '',
};

const hoc = withStyles(styles);

const AlarmLightC = ({
  classes,
  signal,
  iconStyle,
  animatedErr,
  size,
  okIconId,
  errIconId,
  className,
}) =>
  !signal ? (
    <FlexIcon
      className={classnames({ [classes.alarm]: animatedErr, [className]: true })}
      color="error"
      style={{ fontSize: size, ...iconStyle }}
    >
      {errIconId}
    </FlexIcon>
  ) : (
    <FlexIcon color={okColor} className={className} style={{ fontSize: size, ...iconStyle }}>
      {okIconId}
    </FlexIcon>
  );

AlarmLightC.propTypes = propTypes;
AlarmLightC.defaultProps = defaultProps;

export const AlarmLight = hoc(AlarmLightC);
