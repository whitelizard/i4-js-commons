import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { FlexIcon } from './FlexIcon';

const contentHeight = 332;
const titlePadding = 16;
const titleHeight = 64;
const settingsIconSize = 48;
const tooltipIconSize = 30;

const styles = theme => ({
  card: {
    position: 'relative',
    display: 'inline-grid',
    // '&:last-child': { marginBottom: theme.spacing.unit },
    gridTemplateRows: `${titleHeight}px auto`, // enforce title height
  },
  tooltipContainer: {
    position: 'absolute',
    // right: 48,
    top: 8,
  },
  tooltipBody: {
    maxWidth: 180,
  },
  cardContentRoot: { '&:last-child': { paddingBottom: 0 } },
  cardContent: {
    width: '100%',
    padding: 0,
    display: 'flex',
    justifyContent: 'center',
  },
  title: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  settingsMenu: { position: 'absolute', top: titleHeight / 2 - settingsIconSize / 2 },
  tooltip: { position: 'absolute', top: titleHeight / 2 - tooltipIconSize / 2 },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  contentStyle: PropTypes.objectOf(PropTypes.any),
  width: PropTypes.number.isRequired,
  height: PropTypes.number,
  title: PropTypes.string,
  tooltip: PropTypes.string,
  children: PropTypes.node.isRequired,
  // settingsCallback: PropTypes.func,
  menuItems: PropTypes.arrayOf(PropTypes.object),
  style: PropTypes.objectOf(PropTypes.any),
};

const defaultProps = {
  style: {},
  contentStyle: {},
  title: 'Untitled',
  height: contentHeight,
  // settingsCallback: () => {},
  menuItems: undefined,
  tooltip: undefined,
};

class WidgetC extends React.Component {
  state = { anchorEl: undefined, hasError: false };

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  handleMenuItem = method => () => {
    method();
    this.handleMenuClose();
  };

  // handleSettingsCb = action => {
  //   // this.props.settingsCallback(action);
  //   this.handleMenuClose();
  // };

  handleMenuClick = e => {
    e.stopPropagation();
    this.setState({
      anchorEl: e.currentTarget,
    });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: undefined });
  };

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    console.warn(error, info);
  }

  render() {
    const {
      classes,
      title,
      tooltip,
      width,
      height,
      children,
      menuItems,
      style,
      contentStyle,
    } = this.props;
    const { anchorEl, hasError } = this.state;
    const titleWidth =
      width - titlePadding - (tooltip ? settingsIconSize + tooltipIconSize : settingsIconSize);
    const titleFontSize = `${
      title.length >= 20 && width < 400 ? 1.5 - (title.length - 20) / 30 : 1.5
    }rem`;
    return (
      <Card
        className={classes.card}
        style={{ height: (height || contentHeight) + titleHeight, width, ...style }}
      >
        <CardHeader
          classes={{ title: classes.title }}
          style={{ maxWidth: titleWidth }}
          title={
            <Typography
              variant="h5"
              noWrap
              style={{ maxWidth: titleWidth, fontSize: titleFontSize }}
            >
              {title}
            </Typography>
          }
          action={
            menuItems && (
              <div>
                <IconButton
                  aria-label="widget-button"
                  aria-owns={anchorEl ? 'widget-menu' : null}
                  aria-haspopup="true"
                  disabled={hasError}
                  onClick={this.handleMenuClick}
                  className={classes.settingsMenu}
                  style={{ left: width - settingsIconSize }}
                >
                  <FlexIcon icon="more_vert" />
                </IconButton>
                <Menu
                  id="widget-menu"
                  anchorEl={anchorEl}
                  open={!!anchorEl}
                  onClose={this.handleMenuClose}
                  transitionDuration={100}
                >
                  {menuItems.map(conf => (
                    <MenuItem key={conf.label} onClick={this.handleMenuItem(conf.method)}>
                      <ListItemIcon>
                        <FlexIcon icon={conf.icon} color="action" />
                      </ListItemIcon>
                      <ListItemText inset primary={conf.label} />
                    </MenuItem>
                  ))}
                </Menu>
              </div>
            )
          }
        />
        {tooltip && (
          <Tooltip
            id={`${title}WidgetTooltip`}
            title={<div className={classes.tooltipBody}>{tooltip}</div>}
            placement="bottom-end"
          >
            <IconButton className={classes.tooltipContainer} style={{ right: menuItems ? 48 : 8 }}>
              <Icon>help_outline</Icon>
            </IconButton>
          </Tooltip>
        )}
        <CardContent
          className={classes.cardContent}
          classes={{ root: classes.cardContentRoot }}
          style={{ width, ...contentStyle }}
        >
          {hasError ? <h4>Something went wrong.</h4> : children}
        </CardContent>
      </Card>
    );
  }
}

WidgetC.propTypes = propTypes;
WidgetC.defaultProps = defaultProps;

export const Widget = withStyles(styles)(WidgetC); // , { withTheme: true }
