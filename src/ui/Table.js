import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import MuiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const styles = () => ({
  tableContainer: {
    overflowY: 'auto',
    height: '100%',
  },
  cell: { wordBreak: 'break-all' },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  data: PropTypes.objectOf(PropTypes.any),
  style: PropTypes.objectOf(PropTypes.any),
};
const defaultProps = {
  style: {},
  data: {},
};

const hoc = withStyles(styles);

const TableC = ({ style, data, classes }) => {
  if (!data) return null;
  return (
    <div style={style} className={classes.tableContainer}>
      <Divider />
      <MuiTable>
        <TableBody>
          {Object.keys(data).map(heading => (
            <TableRow key={heading}>
              <TableCell padding="dense" variant="head">
                {heading}
              </TableCell>
              <TableCell className={classes.cell}>{data[heading]}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </MuiTable>
    </div>
  );
};

TableC.propTypes = propTypes;
TableC.defaultProps = defaultProps;

export const Table = hoc(TableC);
