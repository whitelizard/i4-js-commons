import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FlexIcon from './FlexIcon';

const styles = () => ({
  icon: { position: 'relative', fontSize: 60, textAlign: 'center' },
  image: {
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
});

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  src: PropTypes.string,
  blackBackground: PropTypes.bool,
  forVideo: PropTypes.bool,
};

const defaultProps = {
  blackBackground: false,
  src: undefined,
  forVideo: false,
  style: {},
};

const __Image = ({ style, src, blackBackground, classes, forVideo }) => {
  // this.renders += 1;
  // if (this.renders > 3 && (!src || this.lastSrc === src)) return null; // prevent render
  // this.lastSrc = src;
  if (!src) {
    return (
      <FlexIcon size="xl" style={{ fontSize: 48, color: blackBackground ? 'white' : 'black' }}>
        {forVideo ? 'videocam_off' : 'mdi-image-off'}
      </FlexIcon>
    );
  }
  const urlIsh =
    (src.startsWith('http') && src) || (src.startsWith('data:') ? src : `data:jpeg;base64,${src}`);
  return (
    <div
      className={classes.image}
      style={{
        backgroundImage: `url(${urlIsh})`,
        ...style,
      }}
    />
  );
};

__Image.propTypes = propTypes;
__Image.defaultProps = defaultProps;

export const Image = withStyles(styles)(__Image);
