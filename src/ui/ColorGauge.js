import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from '@material-ui/core/styles';
import { stringToElement } from 'gauge-animated/lib/gaugeface';
import { createGauge } from 'gauge-animated/lib';

function hexToRgbArray(hexStr) {
  return [0, 0, 0].map((v, i) => parseInt(hexStr.slice(i * 2, 2 * (i + 1)), 16));
}

class ColorGauge extends React.Component {
  static propTypes = {
    theme: PropTypes.objectOf(PropTypes.any).isRequired,
    startColor: PropTypes.string,
    endColor: PropTypes.string,
    size: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    value: PropTypes.number,
    markerColors: PropTypes.func,
    valueDisplayPostfix: PropTypes.string,
  };

  static defaultProps = {
    startColor: 'ff0000',
    endColor: '00ff00',
    size: 500,
    min: 0,
    max: 100,
    value: undefined,
    markerColors: undefined,
    valueDisplayPostfix: '&deg;C',
  };

  componentDidMount() {
    const {
      startColor,
      endColor,
      size,
      min,
      max,
      markerColors,
      valueDisplayPostfix,
      theme,
      ...rest
    } = this.props;
    const steps = max - min;
    const c1 = hexToRgbArray(startColor);
    const c2 = hexToRgbArray(endColor);
    const cDiff = c2.map((v, i) => v - c1[i]);
    const cStep = cDiff.map(v => v / steps);
    const fontFamily = ' ';
    // const fontFamily = theme.typography.fontFamily.split('"')[1];
    const options = {
      size,
      fontFamily,
      scaleLabelFontFamily: fontFamily,
      faceTextFontFamily: fontFamily,
      valueDisplayFontFamily: fontFamily,
      startAngle: Math.PI,
      stopAngle: Math.PI * 2,
      needleAngleMin: Math.PI,
      needleAngleMax: Math.PI * 2,
      labelSteps: 0,
      stopPinColor: 0,
      markerWidth: size / 29,
      markerLength: size * 0.16,
      markerColors:
        markerColors ||
        (i =>
          `rgb(
        ${(c1[0] + cStep[0] * i).toFixed(0)},
        ${(c1[1] + cStep[1] * i).toFixed(0)},
        ${(c1[2] + cStep[2] * i).toFixed(0)})`),
      valueDisplay: true,
      valueDisplayPostfix,
      valueDisplayRadius: -0.2,
      valueDisplayFontSize: size / 7,
      min,
      max,
      stepValue: 1,
      mediumSteps: 0,
      largeSteps: 0,
      needleSvg: siZe => {
        const c = 250;
        const scale = siZe / 500;
        return stringToElement(`
          <svg viewBox="0 0 ${siZe} ${siZe}" xmlns="http://www.w3.org/2000/svg" style="width:${siZe}px; height:${siZe}px;">
            <g>
              <g transform="scale(${scale})">
                <path d="M ${c * 1.6} ${c + 18} L ${c * 1.91} ${c} L ${c * 1.6} ${c -
          18} z" fill="#fff" stroke="#fff"/>
              </g>
            </g>
          </svg>
        `);
      },
      ...rest,
    };
    this.gauge = createGauge(this.div, options);
    this.gauge.setTarget(this.props.value);
  }

  shouldComponentUpdate(nextProps) {
    // console.log('ColorGauge', this.props.value);
    if (nextProps.value !== this.props.value) this.gauge.setTarget(nextProps.value);
    return false;
  }

  render() {
    const { size } = this.props;
    return (
      <div style={{ width: size, height: size * 0.55, overflow: 'hidden' }}>
        <style>{`.gauge-value-display { font-size: ${size / 8}px }`}</style>
        <div
          ref={c => {
            this.div = c;
          }}
        />
      </div>
    );
  }
}
export default withTheme()(ColorGauge);
