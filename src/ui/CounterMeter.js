import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import round from 'lodash.round';
import floor from 'lodash.floor';
import Typography from '@material-ui/core/Typography';

const styles = () => ({
  flexContainer: {
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    zIndex: -1,
  },
  box: { textAlign: 'center' },
});

function textStyleFromText(str, size) {
  const fontSize = Math.max(16, 90 - str.length * 4 - size * 8);
  return { fontSize };
}

// function gradientFromValues(value1, value2, offset, dark) {
//   const range = 255 - offset;
//   const base = offset * +!dark;
//   const r1 = base + floor((value1 * 37) % range);
//   const g1 = base + floor((value1 * 77) % range);
//   const b1 = base + floor((value1 * 119) % range);
//   const r2 = base + floor((value2 * 119) % range);
//   const g2 = base + floor((value2 * 37) % range);
//   const b2 = base + floor((value2 * 77) % range);
//   return `linear-gradient(45deg, rgb(${r1}, ${g1}, ${b1}), rgb(${r2}, ${g2}, ${b2}))`;
// }

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  values: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  config: PropTypes.arrayOf(PropTypes.any),
  // dark: PropTypes.boolean,
  bgOffset: PropTypes.number,
};

const defaultProps = {
  width: 300,
  height: undefined,
  values: [],
  config: [],
  // dark: false,
  bgOffset: 230,
};

// const contextTypes = {
//   lang: PropTypes.string.isRequired,
// };

class CounterMeterC extends React.Component {
  // state = {
  //   swap: 0,
  // };

  shouldComponentUpdate(nextProps) {
    const { values = [] } = nextProps;
    // console.log('values in shouldComponentUpdate', values);
    return values.length !== 0 && !values.some(v => typeof v === 'undefined' || Number.isNaN(v));
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   return { swap: (-Math.sign(prevState.swap - 0.5) + 1) / 2 };
  // }

  render() {
    const { width, height, config, values, bgOffset, classes } = this.props;
    // const { swap } = this.state;
    // const second = values.length > 1 ? values[1] : values[0];
    // const lastBg = this.bg;
    // const bg = gradientFromValues(values[0], second, bgOffset);
    return (
      <div style={{ width, height }} className={classes.flexContainer}>
        {/* <div
          className={classes.bg}
          style={{
            background: swap ? lastBg || 'white' : bg,
          }}
        />
        <div
          style={{
            background: swap ? bg : lastBg || 'white',
            opacity: swap,
            transition: 'all .4s ease',
          }}
        /> */}
        {values.map((v, i) => {
          let value = v;
          const { unit = '', transform, decimals, floor: toFloor, label } = config[i] || {};
          if (config[i]) {
            if (transform) {
              if (typeof transform === 'function')
                try {
                  value = config[i].transform(value);
                } catch (err) {
                  console.warn(
                    'CounterMeter - config[x].transform failed. Value:',
                    value,
                    'Transform:',
                    transform,
                  );
                }
              else
                console.warn(
                  'CounterMeter - config[x].transform not of type function. transform is:',
                  transform,
                );
            }
            if (typeof decimals === 'number') {
              value = toFloor ? floor(value, decimals) : round(value, decimals);
            }
            if (typeof value !== 'string' && typeof value !== 'undefined' && !Number.isNaN(value)) {
              value = value.toLocaleString();
            }
          }
          return (
            <div
              key={label || i}
              className={classes.box}
              style={{ marginBottom: Math.max(10, 30 - values.length * 4) }}
            >
              <span style={textStyleFromText(`${value}${unit}`, values.length)}>
                {typeof value === 'undefined' || (Number.isNaN(value) && typeof value !== 'string')
                  ? '-'
                  : `${value}${unit}`}
              </span>
              <br />
              <Typography>{label || <span>&nbsp;</span>}</Typography>
            </div>
          );
        })}
      </div>
    );
  }
}

CounterMeterC.propTypes = propTypes;
CounterMeterC.defaultProps = defaultProps;

export const CounterMeter = withStyles(styles)(CounterMeterC);
