import Qty from 'js-quantities';
import { nilOrEmpty } from './transforms';

export const units = {
  // kind: ['metric', 'imperial', 'option #3', 'option #4', ...]
  length: [{ name: 'Meters', abbv: 'm' }, { name: 'Feets', abbv: 'ft' }],
  area: [{ name: 'Square meters', abbv: 'm2' }, { name: 'Square feets', abbv: 'ft2' }],
  volume: [{ name: 'Liters', abbv: 'l' }, { name: 'Fluid-ounces', abbv: 'floz' }],
  temperature: [{ name: 'Celsius', abbv: 'tempC' }, { name: 'Fahrenheit', abbv: 'tempF' }],
  pressure: [
    { name: 'Pascal', abbv: 'MPa' },
    { name: 'psi', abbv: 'psi' },
    { name: 'Bars', abbv: 'bar' },
  ],
};

// LocalStorage helpers.
const store = obj => localStorage.setItem('units', JSON.stringify(obj));
const load = () => {
  try {
    return JSON.parse(localStorage.getItem('units')) || {};
  } catch {
    return {};
  }
};

// Use metric accessors.
export const setUseMetric = useMetric => store(Object.assign(load(), { useMetric }));
export const useMetric = appConfigUseMetric => {
  const value = load();
  return typeof value.useMetric === 'boolean' ? value.useMetric : appConfigUseMetric;
};

// User kind options accessors.
export const setUserKindOption = (kind, option) => {
  const value = load();
  if (nilOrEmpty(option)) {
    if (value.kinds) delete value.kinds[kind];
  } else {
    if (!value.kinds) value.kinds = {};
    value.kinds[kind] = option;
  }
  store(value);
};

export const userKindOption = kind => {
  const value = load();
  return value.kinds ? value.kinds[kind] : undefined;
};

// kind -> kind.
const repKind = (metric, kind) => {
  const toUnit = userKindOption(kind);
  if (toUnit) return toUnit;

  if (units[kind]) return units[kind][metric ? 0 : 1].abbv;
  return undefined;
};

const represent = metric => (val, unit) => {
  const q = Qty(val, unit);
  const toUnit = repKind(metric, q.kind());
  return toUnit ? q.to(toUnit) : q.toBase();
};

// (value, 'unit') => value.
export const asMetric = represent(true);
export const asImperial = represent(false);
export const as = represent(useMetric());

// Get user specific unit from unit or kind.
export const getUnit = fromUnit => as(1, fromUnit).units();
export const getUnitByKind = kind => getUnit(Qty.getUnits(kind)[0]);

// From SI units to user specific.
export const fromBase = (value, kind) =>
  as(
    value,
    Qty(1, Qty.getUnits(kind)[0])
      .toBase()
      .units(),
  );
