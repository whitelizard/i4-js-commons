import { useEffect } from 'react';

export const useScrollThresholdToggle = (threshold, toggler) => {
  useEffect(() => {
    toggler(false);
    window.onscroll = () => {
      const scrolled =
        document.body.scrollTop > threshold || document.documentElement.scrollTop > threshold;
      if (scrolled) {
        toggler(true);
      } else {
        toggler(false);
      }
    };
    return () => {
      toggler(true);
      window.onscroll = Function.prototype;
    };
  }, []);
};
