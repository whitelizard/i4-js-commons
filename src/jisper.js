import get from 'lodash.get';
import set from 'lodash.set';
import getTransformer from 'json-transformer-js';
import { functionalParserWithVars } from 'json-rule-processor/dist/minimal-lisp-parser';
// import { stringToElement } from 'gauge-animated/lib/gaugeface';
import { connect } from 'react-redux';
import * as R from 'ramda';
import { req } from './be-client';
import * as dataUtils from './data-utils';
import * as dmUtils from './dm-utils';
import * as uiUtils from './ui-utils';
import { withChannelData } from './withChannelData';

export const confTransformer = R.curry(
  ({ staticContext, transforms, jispParserMutator }, context) => {
    const jisp = functionalParserWithVars(context, { envExtra: staticContext });
    if (jispParserMutator) jispParserMutator(jisp);
    const transformer = getTransformer({
      defaultLevel1Transform: (v, k) => get(set(context, k, v), k),
      transforms: {
        '%ml%': arr => {
          try {
            return jisp.evalWithLog(arr);
          } catch (err) {
            console.error('jisp.eval:', err);
            return undefined;
          }
        },
        ...transforms,
      },
    });
    return transformer;
  },
);

export const transformConf = R.curry(({ staticContext, transforms }, context, configJson) => {
  if (!configJson) return undefined;
  const transformer = confTransformer(
    {
      staticContext: {
        '%': R.modulo,
        tapLog: R.tap(x => console.log('TAPPING:', x)),
        withChannelData,
        connect,
        utils: { ...dataUtils, ...dmUtils, ...uiUtils },
        // stringToElement,
        req,
        ...staticContext,
      },
      transforms,
    },
    context,
  );
  return transformer(configJson);
});
