/**
 * TODO: Always transform ts to a date, for everything coming from the outside
 */
import getClient, { ds } from 'extended-ds-client';
import sha256 from 'crypto-js/sha256';
import * as R from 'ramda';
import Tiip from 'jstiip';
import subSeconds from 'date-fns/fp/subSeconds';
import { dateToTs } from './dm-utils';

/**
 * Publishes a message through the backend.
 * @param {string} channel - The channel to publish on.
 * @param {Array} [pl=[]] - The payload to send.
 * @param {string} [sig] - The signal data to apply to the published message.
 */
export const pub = (channel, pl = [], sig) => {
  const msg = new Tiip({ pl });
  if (sig) msg.sig = sig;
  const msgOut = msg.toJS();
  ds.client.event.emit(channel, msgOut);
  // console.log('PUB:', channel, msgOut);
};

/**
 * Subscribes to a channel on the backend.
 * @param  {function} callback - The callback to trigger when a message arrives.
 * @param  {string} channel - The channel to subscribe to.
 */
export const sub = R.curry((callback, channel) => ds.client.event.subscribe(channel, callback));

/**
 * Unsubscribes from a channel on the backend.
 * @param  {string} channel - The channel to unsubscribe from.
 * @param  {function} [callback] - The callback that was previously registered with subscribe. If
 *  the callback is omitted, all subscriptions for the channel will be removed.
 */
export const unsub = (channel, callback) => ds.client.event.unsubscribe(channel, callback);

const confUpdatesChannel = R.curry((type, rid) => `conf/${type}/update/${rid}`);
const busChannel = rid => `data/${rid}`;

export const subEntityUpdates = type => {
  const ridToChannel = confUpdatesChannel(type);
  return cb => R.forEach(R.o(sub(cb), ridToChannel));
};
export const unsubEntityUpdates = type => {
  const ridToChannel = confUpdatesChannel(type);
  return (rids, cb) => R.forEach(R.o(ch => unsub(ch, cb), ridToChannel))(rids);
};

/**
 * Performs a request to the backend.
 * @param  {string} id - The id of the rpc
 * @param  {Object} [args={}] - Arguments passed on to the rpc
 * @return {any} - Depends on the rpc.
 */
// TODO: Refactor temporarily to rids:* to target all, but only for 'conf...'-requests
export const req = R.curry((id, args) =>
  ds.client.rpc.p.make(id, args).catch(err => {
    console.error(id, args, err);
    throw err;
  }),
);

export const fromChannel = ({ rid, last, tail, ttl, cb, noSub }) => {
  if (last || tail) {
    req('data-history/read', {
      channel: rid,
      ...(ttl ? { from: dateToTs(subSeconds(ttl)(new Date())) } : {}),
      limit: tail || Number(last),
    }).then(reply => reply.reverse().forEach(msg => cb(msg)));
  }
  if (!noSub) {
    const channel = busChannel(rid);
    sub(cb, channel);
    return channel;
    // this.subscriptions = R.append(busChannel)(this.subscriptions);
  }
  return false;
};

// /**
//  * Converts a rid to be url-friendly
//  * @param {string} rid - The record id to convert.
//  * @return {string} - An url-friendly rid.
//  */
// export const ridToUrl = R.compose(
//   R.replace(':', '-'),
//   R.replace('#', ''),
// );
//
// /**
//  * Converts a url-friendly rid to an actual rid.
//  * @param {string} rid - The url-friendly record id to convert.
//  * @return {string} - An actual record id.
//  */
// export const urlToRid = R.compose(
//   rid => `#${rid}`,
//   R.replace('-', ':'),
// );

export const logout = () => {
  if (!ds.client) return;
  ds.client.off();
  ds.client.close();
};

/**
 * Creates a connection to the backend server.
 * @param {string} serverAddress
 * @param {function} onConnectionError - Callback triggered on connection errors.
 * @param {function} onUnexpectedError - Callback triggered on general errors in the client.
 * @param {}
 * @return {Promise} - [description]
 */
// TODO: What does this function return? client? as promise?
const createConnection = async (serverAddress, onConnectionError, onUnexpectedError) => {
  console.log('createConnection:', serverAddress, ds.client);
  if (ds.client) logout();
  ds.client = getClient(serverAddress);
  ds.client.on('error', onUnexpectedError);
};

export const pwHash = pw => String(sha256(pw));

export const login = async (serverAddress, id, password, onConnectionError, onUnexpectedError) => {
  createConnection(serverAddress, onConnectionError, onUnexpectedError);
  await ds.client.p.login({ id, password });
  ds.client.on('connectionStateChanged', state => {
    // We need to set this only when we know that we got a working logged in connection
    console.log('CONNECTION STATE:', state);
    if (state === 'ERROR' && ds.client.connectionState !== 'ERROR') onConnectionError();
    ds.client.connectionState = state;
  });
  return { id, password, serverAddress };
};

// const ensureRids = ensureKey('rids', []);
//
// export const fetchEntity = type => {
//   if (!fetchMapping[type]) throw new Error('No such entity type:', type);
//   return async (args = {}) => {
//     let records = [];
//     try {
//       records = await req(`conf/${fetchMapping[type]}`, ensureRids(args));
//     } catch (err) {
//       console.warn(`Fetching ${type}s failed:`, err);
//     }
//     return R.indexBy(R.prop('rid'), records);
//   };
// };
