import * as R from 'ramda';
import update from 'immutability-helper';
import stringify from 'json-stringify-pretty-compact';
// import formatDistance from 'date-fns/fp/formatDistance';
// import addSeconds from 'date-fns/fp/addSeconds';
import { formatDistance, addSeconds } from 'date-fns/esm/fp';
import { toTitle } from './ui-utils';
// import debounce from 'lodash.debounce';
const debounce = require('lodash.debounce');

export const docFromId = id =>
  R.compose(
    R.find(R.propEq('id', id)),
    R.values,
    R.when(R.isNil, () => {}),
  );

export const fromIdGetRid = id =>
  R.compose(
    R.prop('rid'),
    docFromId(id),
  );

export const recordToRestJson = R.curry(
  (toOmit, doc) => stringify(R.omit(toOmit)(doc), { maxLength: 100 }),
  // JSON.stringify(R.omit(toOmit)(doc), undefined, 2),
);

export const filterOnType = type => R.filter(R.propEq('type', type));

export const isJsonOfType = type =>
  R.tryCatch(
    R.compose(
      R.is(type),
      JSON.parse,
    ),
    R.F,
  );
export const isJsonObject = isJsonOfType(Object);

export const createResizeEventFunc = R.memoizeWith(
  // force format break!
  R.identity,
  limit => debounce(() => setTimeout(() => window.dispatchEvent(new Event('resize')), limit)),
);

/**
 * Generates a string of characters including numbers and letters in upper and lower case.
 * @param {number} length - The length of the string to generate.
 * @return {string} The generated string.
 */
export function generateString(length = 14) {
  const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let result = '';
  for (let i = 0, n = charset.length; i < length; i += 1) {
    result += charset.charAt(Math.floor(Math.random() * n));
  }
  return result;
}

/**
 * Validates an email address.
 * @param  {string} email - The email to validate
 * @return {boolean}
 */
export function validateEmail(email) {
  // eslint-disable-next-line
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

// TODO: replace is not working, why?
// export const get = R.compose(
//   R.path,
//   R.map(x => (Number.isNaN(Number(x)) ? x : Number(x))),
//   R.split('.'),
//   str => str.replace(/\[/g, '.'),
//   R.replace(/\]/g, '.'),
// );

export const secondsToFormattedDistance = R.compose(
  toTitle,
  formatDistance(new Date()),
  x => addSeconds(x, new Date()),
  R.when(R.isNil, () => 0),
);

export const utcDateToDate = d =>
  new Date(
    Date.UTC(
      d.getUTCFullYear(),
      d.getUTCMonth(),
      d.getUTCDay(),
      d.getUTCHours(),
      d.getUTCMinutes(),
      d.getUTCSeconds(),
      d.getUTCMilliseconds(),
    ),
  );

export const createProgressiveStepper = (intervalLength = 10, progressions = []) => x => {
  const interval = Math.floor(x / intervalLength);
  const inInterval = x % intervalLength;
  const toMax = progressions.slice(0, interval);
  const offset = intervalLength * toMax.reduce((acc, a) => acc + a, 0);
  return offset + progressions[interval] * inInterval;
};

const oldConfigMerge = R.mergeDeepWithKey((key, left, right) =>
  R.includes(key, ['supported', 'menuItems', 'availableRoutes', 'services'])
    ? R.concat(left, right)
    : right,
);

export const mergeConfig = (baseConfig, updates) => {
  let mergedConfig;
  try {
    mergedConfig = update(baseConfig, updates);
  } catch (err) {
    mergedConfig = oldConfigMerge(baseConfig, updates);
  }
  return mergedConfig;
};
