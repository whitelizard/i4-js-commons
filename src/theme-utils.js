export const drawerWidthDefault = 240;

export const isMenuShown = (theme, width) => width > theme.breakpoints.values.md;

export const contentWidth = (theme, width, drawerWidth = drawerWidthDefault) =>
  isMenuShown(theme, width) ? width - drawerWidth : width;

// TODO: Should replicate theme.mixins.toolbar correctly!
export const appBarHeight = (theme, width) =>
  width < theme.breakpoints.width('sm')
    ? theme.mixins.toolbar.minHeight
    : theme.mixins.toolbar['@media (min-width:600px)'].minHeight;

export const contentHeight = (theme, width, height) => height - appBarHeight(theme, width);
