import * as R from 'ramda';
// import Tiip from 'jstiip';

export const ensureTsV3 = msg => {
  const { ct } = msg;
  if (ct !== undefined) {
    if (Object.prototype.toString.call(ct) === '[object Date]') return { ...msg, ts: ct };
    return { ...msg, ts: new Date(Number(ct) * 1000).toISOString() };
  }
  // if (Object.prototype.toString.call(ts) === '[object Date]') return msg;
  return msg;
};

export const dateToTs = da => da.toISOString();
export const tsToDate = ts => new Date(ts);
export const tsToPl = msg => R.prepend(msg.ts, msg.pl);
export const evolveTsToDate = R.evolve({ ts: tsToDate });
export const confUpdatesChannel = R.curry((type, rid) => `conf/${type}/update/${rid}`);
export const busChannel = rid => `data/${rid}`;
