import React from 'react';
import * as R from 'ramda';
import get from 'lodash.get';
import { pub, unsub, fromChannel } from './be-client';
import { evolveTsToDate, ensureTsV3 } from './dm-utils';
// const get = require('lodash.get');

const act = conf => pl => conf.forEach(ch => pub(ch, pl));

function confToChannelNames(conf) {
  if (conf.channel && conf.channel.id) return [conf.channel.id];
  if (conf.channels) {
    return conf.channels.reduce((r, cc) => {
      if (cc.id) r.push(cc.id);
      return r;
    }, []);
  }
  return [];
}

// const testConf = {
//   subs: {
//     temp1: { rid: 'x', tail: 10, ttl: 86000 },
//     temp2: { rid: 'y', tail: 10, ttl: 86000 },
//   },
//   props: {
//     payload: {
//       // init: []
//       grab: 'temp1.pl',
//       // grab: ['concat', ['temp1.pl', 'temp2.pl']],
//     },
//   },
// };

export const withChannelData = keyChannelMap => Component => {
  // const WithChannelData = props => {
  //   const [states, setStates] = useState({});
  //   const actuators = useRef({});
  //   const subscriptions = useRef([]);
  //   const element = useRef();
  //
  //   return <Component {...props} {...states} {...actuators.current} ref={element} />;
  // };

  class WithChannelData__ extends React.Component {
    // directDomCallbacks = []; // TODO: future idea of optimization (BAD pattern!)
    constructor(props) {
      super(props);
      this.actuators = {};
      this.subscriptions = [];
      if (!keyChannelMap || typeof keyChannelMap !== 'object') {
        console.warn('WithChannelData keyChannelMap:', keyChannelMap);
        return;
      }
      this.state = Object.entries(keyChannelMap).reduce(
        (r, [key, conf]) => (conf.init !== undefined ? { ...r, [key]: conf.init } : r),
        {},
      );
      // console.log('withChannelData state init:', this.state);
    }

    componentDidMount() {
      if (!keyChannelMap || typeof keyChannelMap !== 'object') return;
      Object.entries(keyChannelMap).forEach(([key, conf]) => {
        // console.log('keyChannelMap key conf:', key, conf);
        if (conf.actuator) {
          this.actuators[key] = act(confToChannelNames(conf));
        } else if (conf.subs) {
          conf.subs.forEach(channel => {
            if (!channel || channel.rid === undefined) return;
            const { path, transform } = channel;
            const subdChannel = fromChannel({
              ...channel, // rid, last, tail, ttl
              cb: this.onMsg({
                key,
                path,
                transform: transform || conf.transform,
              }),
            });
            if (subdChannel) this.subscriptions = R.append(subdChannel)(this.subscriptions);
          });
        }
      });
    }

    componentWillUnmount() {
      this.subscriptions.forEach(busChannel => unsub(busChannel));
    }

    onMsg({ transform, key, path }) {
      return message => {
        const msg = evolveTsToDate(ensureTsV3(message));
        // console.log('onMsg', path, typeof transform, transform, key);
        const data = path ? get(msg, path) : msg;
        // if (transform && msg) {
        //   console.log('onMsg:', typeof transform, transform, key);
        // }
        this.setState(state => {
          const { [key]: prevProp } = state;
          if (data === undefined) return undefined;
          const value = transform ? transform(data, prevProp) : data;
          // if (transform && msg) {
          //   console.log('onMsg tr:', value);
          // }
          if (value === undefined) return undefined;
          return { [key]: value };
        });
      };
    }

    render() {
      // console.log('withChannelData render:', this.props, this.state);
      return (
        <Component
          {...this.props}
          {...this.state}
          {...this.actuators}
          // TODO: future idea of optimization (BAD pattern!)
          // directDomCallbacks={this.directDomCallbacks}
          // ref={c => (this.element = c)}
        />
      );
    }
  }
  return WithChannelData__;
};
