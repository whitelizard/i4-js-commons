import * as R from 'ramda';

export const contrastThreshold = 145;
export const noneOptionKey = '_%NONE%_';

export const typeToUnit = {
  temperature: '°C',
  weight: 'kg',
  force: 'N',
  length: 'mm',
  pressure: 'bar',
};
export const toTitle = str => str.replace(/^./g, char => char.toUpperCase());
export const unCamel = str => str.replace(/([A-Z])/g, (all, ch) => ` ${ch.toLowerCase()}`);
export const camelToLabel = R.compose(
  toTitle,
  unCamel,
);
// export default toTitle;

const hexToRgb = hex => {
  let r;
  let g;
  let b;
  if (hex) {
    const color = hex.charAt(0) === '#' ? hex.substr(1) : hex;
    r = color.charAt(0) + color.charAt(1);
    g = color.charAt(2) + color.charAt(3);
    b = color.charAt(4) + color.charAt(5);
    r = parseInt(r, 16);
    g = parseInt(g, 16);
    b = parseInt(b, 16);
  }
  return { r, g, b };
};

export const prettifyInfoValue = R.when(
  R.is(Object),
  R.ifElse(R.has('type'), v => `${v.pl} ${typeToUnit[v.type]}`, R.prop('pl')),
);

export const brightness = hex => {
  const { r, g, b } = hexToRgb(hex);
  return (r * 299 + g * 587 + b * 114) / 1000;
};
export const brighterThan = (hex, x) => brightness(hex) > x;

export const highlightedText = (primary, secondary, bg = '#FFFFFF') => {
  const threshold = brightness(bg) - (255 - contrastThreshold);
  let textColor = primary;
  if (brighterThan(textColor, threshold)) textColor = secondary;
  if (brighterThan(textColor, threshold)) textColor = 'black';
  return textColor;
};

// Prettier text of entity props
export const prettyPrint = item => {
  if (R.is(Object, item)) {
    return JSON.stringify(item);
  }
  // if (Array.isArray(item)) {
  //   const val = item.map(i => prettyPrint(i));
  //   return `${val.join(', ')}`;
  // }
  // if (R.is(Object, item)) {
  //   const row = R.toPairs(item).reduce((arr, v, k) => arr.concat(`${k}: ${v}`), []);
  //   return `{ ${row.join(', ')} }`;
  // }
  if (typeof item === 'number') return item;
  return item ? item.toString() : false;
};
