/**
 * Highly generic/reusable short functions/transforms/utilities (non-app-specific).
 * Should probably only depend on ramda (or future replacement equivalent).
 */
import * as R from 'ramda';

export const hasnt = R.complement(R.has);
export const nilOrEmpty = R.converge(R.or, [R.isNil, R.isEmpty]);

export const awaitWith = R.curry((then, promise) => promise.then(then));
export const catchWith = R.curry((catcher, promise) => promise.catch(catcher));

export const ensureKey = (key, defaultValue) => R.when(hasnt(key), R.assoc(key, defaultValue));
export const withEventValue = fn => ev => fn(ev.target.value);
export const payloadIntoKey = key => (state, action) => ({ ...state, [key]: action.payload });
