import sha256 from 'crypto-js/sha256';
import { createAction, handleActions } from 'redux-actions';
import * as R from 'ramda';
import { req } from './be-client';
import { ensureKey } from './transforms';

const defaultState = {};
const ensureRids = R.compose(
  ensureKey('rids', []),
  R.when(R.isNil, R.always({})),
);

export const logRequestError = (...rpcs) => err =>
  console.warn(`Request to "${rpcs.join('|')}" failed -`, err);
export const tapLogRequestError = (...rpcs) => err => {
  logRequestError(...rpcs)(err);
  throw err;
};

export const createEntityActions = type => ({
  merge: createAction(`${type}/merge`),
  set: createAction(`${type}/set`),
  delete: createAction(`${type}/delete`),
});

const formatRecords = R.indexBy(R.prop('rid'));
export const formatAndDispatchMerge = entityActions => dispatch =>
  R.compose(
    R.prop('payload'),
    dispatch,
    entityActions.merge,
    formatRecords,
    R.when(x => !Array.isArray(x), R.of),
  );

export const createReadIdentityAction = (rpcId, entityActions) => args => async dispatch => {
  const recordsObj = await req(rpcId, ensureRids(args));
  let connectionStatuses = {};
  try {
    connectionStatuses = await req('presence/listClients', {
      filter: R.compose(
        R.values,
        R.pluck('id'),
      )(recordsObj),
    });
  } catch (err) {
    console.warn('Fetching connectionStatuses failed:', err);
  }
  const recordsWithConnectionStatus = R.map(
    R.converge(R.assoc, [
      () => 'connectionStatus',
      R.compose(
        R.flip(R.prop)(connectionStatuses),
        R.prop('id'),
      ),
      R.identity,
    ]),
  )(recordsObj);
  return formatAndDispatchMerge(entityActions)(dispatch)(recordsWithConnectionStatus);
};

export const createEntityActionCreator = (argsTransform = R.identity) => (
  rpcId,
  entityActions,
) => args => async dispatch =>
  req(rpcId, argsTransform(args))
    .then(formatAndDispatchMerge(entityActions)(dispatch))
    .catch(tapLogRequestError(rpcId));

export const createCreateEntityAction = createEntityActionCreator();
export const createReadEntityAction = createEntityActionCreator(ensureRids);

const hashPwInArgs = args => ({ ...args, password: String(sha256(args.password)) });
export const createCreateIdentityAction = createEntityActionCreator(hashPwInArgs);

export const createUpdateEntityAction = (
  rpcId,
  entityActions,
  readAction,
) => args => async dispatch =>
  req(rpcId, args)
    .then(count => {
      const { rids, properties, ...rest } = args;
      if (rids.length === count)
        return R.compose(
          formatAndDispatchMerge(entityActions)(dispatch),
          R.map(rid => ({ rid, ...properties })),
        )(rids);
      return readAction({ rids, ...rest });
    })
    .catch(tapLogRequestError(rpcId));

export const createDeleteEntityAction = (rpcId, entityActions) => args => async dispatch =>
  req(rpcId, args)
    .then(() => dispatch(entityActions.delete(args.rids)))
    .catch(tapLogRequestError(rpcId));

export const createEntityReducer = entityActions =>
  handleActions(
    {
      [entityActions.merge]: (state, action) => ({ ...state, ...action.payload }),
      [entityActions.set]: (state, action) => action.payload,
      [entityActions.delete]: (state, action) => R.omit(action.payload, state),
    },
    defaultState,
  );
