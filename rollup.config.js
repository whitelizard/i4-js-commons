import resolve from 'rollup-plugin-node-resolve';
// import buble from 'rollup-plugin-buble';
import globals from 'rollup-plugin-node-globals';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
// import pkg from './package.json';
import multiInput from 'rollup-plugin-multi-input';
import postcss from 'rollup-plugin-postcss';

export default {
  input: ['src/**/*.js'],
  output: {
    format: 'esm',
    dir: 'dist',
  },
  plugins: [
    // buble({ transforms: { asyncAwait: false }, objectAssign: Object.assign }),
    multiInput(),
    postcss({
      extensions: ['.css'],
    }),
    babel({
      exclude: 'node_modules/**',
      presets: ['@babel/env', '@babel/preset-react'],
      plugins: ['transform-class-properties'],
    }),
    resolve(),
    commonjs({
      include: 'node_modules/**',
    }), // so Rollup can convert `ms` to an ES module
    globals(),
  ],
  // external: ['react', 'prop-types'],
  // globals: {
  //   react: 'React',
  // },
};
//
// export default [
//   // browser-friendly UMD build
//   {
//     input: 'src/*',
//     output: {
//       name: 'i4-js-commons',
//       file: 'dist/umd/',
//       format: 'umd',
//     },
//     plugins: [
//       resolve(), // so Rollup can find `ms`
//       commonjs(), // so Rollup can convert `ms` to an ES module
//     ],
//   },
//
//   // CommonJS (for Node) and ES module (for bundlers) build.
//   // (We could have three entries in the configuration array
//   // instead of two, but it's quicker to generate multiple
//   // builds from a single configuration where possible, using
//   // an array for the `output` option, where we can specify
//   // `file` and `format` for each target)
//   {
//     input: 'src/',
//     external: ['ramda'],
//     output: [{ file: 'dist/cjs/', format: 'cjs' }, { file: 'dist/esm/', format: 'es' }],
//   },
// ];
